package ru.tsc.kirillov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class ProjectRemoveByIdRequest extends AbstractIdRequest {

    public ProjectRemoveByIdRequest(@Nullable final String token, @Nullable final String id) {
        super(token, id);
    }

}
