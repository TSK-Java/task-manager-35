package ru.tsc.kirillov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class TaskRemoveByIndexRequest extends AbstractIndexRequest {

    public TaskRemoveByIndexRequest(@Nullable final String token, @Nullable final Integer index) {
        super(token, index);
    }

}
