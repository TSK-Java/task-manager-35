package ru.tsc.kirillov.tm.exception.system.value;

import lombok.NoArgsConstructor;
import ru.tsc.kirillov.tm.exception.AbstractException;

@NoArgsConstructor
public abstract class AbstractValueException extends AbstractException {

    public AbstractValueException(final String message) {
        super(message);
    }

    public AbstractValueException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public AbstractValueException(final Throwable cause) {
        super(cause);
    }

    public AbstractValueException(
            final String message,
            final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
