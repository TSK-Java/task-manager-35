package ru.tsc.kirillov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.model.Task;

@NoArgsConstructor
public class TaskGetByIndexResponse extends AbstractTaskResponse {

    public TaskGetByIndexResponse(@Nullable final Task task) {
        super(task);
    }

}
