package ru.tsc.kirillov.tm.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApplicationVersionResponse extends AbstractResponse {

    private String version;

}
