package ru.tsc.kirillov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public enum Status {

    NOT_STARTED("Не запущено"),
    IN_PROGRESS("Выполняется"),
    COMPLETED("Завершено");

    @Getter
    @NotNull
    private final String displayName;

    Status(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @Nullable
    public static Status toStatus(@Nullable final String value) {
        if (value == null || value.isEmpty())
            return null;
        for (Status status: values()) {
            if (status.name().equals(value))
                return status;
        }

        return null;
    }

    @NotNull
    public static String toName(@Nullable Status status) {
        if (status == null) return "";
        return status.getDisplayName();
    }

}
