package ru.tsc.kirillov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataJsonFasterXmlLoadRequest extends AbstractUserRequest {

    public DataJsonFasterXmlLoadRequest(@Nullable final String token) {
        super(token);
    }

}
