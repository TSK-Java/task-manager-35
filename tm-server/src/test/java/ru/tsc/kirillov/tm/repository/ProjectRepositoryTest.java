package ru.tsc.kirillov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import ru.tsc.kirillov.tm.api.repository.IProjectRepository;
import ru.tsc.kirillov.tm.enumerated.Sort;
import ru.tsc.kirillov.tm.model.Project;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public final class ProjectRepositoryTest {

    @NotNull final IProjectRepository repository = new ProjectRepository();

    @NotNull final String userId = UUID.randomUUID().toString();

    @NotNull final String projectName = UUID.randomUUID().toString();

    @NotNull final String projectDescription = UUID.randomUUID().toString();

    @After
    public void finalization() {
        repository.clear();
    }

    @Test
    public void add() {
        Assert.assertEquals(0, repository.count());
        @NotNull final Project project = new Project();
        project.setName(projectName);
        project.setUserId(userId);
        repository.add(project);
        Assert.assertEquals(1, repository.count());
        @Nullable final Project projectFind = repository.findOneById(project.getId());
        Assert.assertNotNull(projectFind);
        Assert.assertEquals(project, projectFind);
        Assert.assertEquals(project.getName(), projectFind.getName());
    }

    @Test
    public void addByUserId() {
        Assert.assertEquals(0, repository.count());
        @NotNull final Project project = new Project();
        project.setName(projectName);
        project.setUserId(userId);
        repository.add(userId, project);
        Assert.assertEquals(1, repository.count());
        Assert.assertEquals(1, repository.count(userId));
        Assert.assertEquals(0, repository.count(UUID.randomUUID().toString()));
        @Nullable final Project projectFind = repository.findOneById(userId, project.getId());
        Assert.assertNotNull(projectFind);
        Assert.assertEquals(project, projectFind);
        Assert.assertEquals(project.getName(), projectFind.getName());
    }

    @Test
    public void addList() {
        Assert.assertEquals(0, repository.count());
        @NotNull List<Project> projects = new ArrayList<>();
        final int countProject = 10;
        for (int i = 0; i < countProject; i++) {
            @NotNull final String userId = UUID.randomUUID().toString();
            @NotNull final String projectName = UUID.randomUUID().toString();
            @NotNull final Project project = new Project();
            project.setName(projectName);
            project.setUserId(userId);
            projects.add(project);
        }
        repository.add(projects);
        Assert.assertEquals(countProject, repository.count());
        for (int i = 0; i < countProject; i++) {
            @NotNull final Project project = projects.get(i);
            Assert.assertNull(repository.findOneById(UUID.randomUUID().toString(), project.getId()));
            @Nullable final Project projectFind = repository.findOneById(project.getId());
            Assert.assertNotNull(projectFind);
            Assert.assertEquals(projects.get(i).getName(), projectFind.getName());
        }
    }

    @Test
    public void create() {
        Assert.assertEquals(0, repository.count());
        @NotNull final Project project = repository.create(userId, projectName);
        Assert.assertEquals(1, repository.count());
        Assert.assertEquals(1, repository.count(userId));
        Assert.assertEquals(0, repository.count(UUID.randomUUID().toString()));
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString(), project.getId()));
        @Nullable final Project projectFind = repository.findOneById(userId, project.getId());
        Assert.assertNotNull(projectFind);
        Assert.assertEquals(project, projectFind);
        Assert.assertEquals(project.getName(), projectFind.getName());
    }

    @Test
    public void createDescription() {
        Assert.assertEquals(0, repository.count());
        @NotNull final Project project = repository.create(userId, projectName, projectDescription);
        Assert.assertEquals(1, repository.count());
        Assert.assertEquals(1, repository.count(userId));
        Assert.assertEquals(0, repository.count(UUID.randomUUID().toString()));
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString(), project.getId()));
        @Nullable final Project projectFind = repository.findOneById(userId, project.getId());
        Assert.assertNotNull(projectFind);
        Assert.assertEquals(project, projectFind);
        Assert.assertEquals(project.getDescription(), projectFind.getDescription());
    }

    @Test
    public void clear() {
        Assert.assertEquals(0, repository.count());
        repository.create(userId, projectName);
        Assert.assertEquals(1, repository.count());
        Assert.assertEquals(1, repository.count(userId));
        Assert.assertEquals(0, repository.count(UUID.randomUUID().toString()));
        repository.clear();
        Assert.assertEquals(0, repository.count());
        Assert.assertEquals(0, repository.count(userId));
        Assert.assertEquals(0, repository.count(UUID.randomUUID().toString()));
    }

    @Test
    public void findAll() {
        repository.create(userId, projectName);
        Assert.assertEquals(1, repository.findAll().size());
    }

    @Test
    public void findAllUserId() {
        repository.create(userId, projectName);
        @Nullable final String userNullId = null;
        Assert.assertEquals(0, repository.findAll(userNullId).size());
        Assert.assertEquals(0, repository.findAll(UUID.randomUUID().toString()).size());
        Assert.assertEquals(1, repository.findAll(userId).size());
    }

    @Test
    public void findAllComparatorNullUserId() {
        repository.create(userId, projectName);
        @Nullable final String userNullId = null;
        Assert.assertEquals(0, repository.findAll(userNullId, Sort.BY_NAME.getComparator()).size());
    }

    @Test
    public void findAllComparator() {
        final int countProject = 10;
        for (int i = 0; i < countProject; i++) {
            @NotNull final String projectName = String.format("Project_%d", countProject - i - 1);
            repository.create(userId, projectName);
        }
        @NotNull List<Project> projects = repository.findAll(userId, Sort.BY_NAME.getComparator());
        Assert.assertEquals(countProject, projects.size());
        for (int i = 0; i < countProject; i++) {
            @NotNull final String projectName = String.format("Project_%d", i);
            Assert.assertEquals(projectName, projects.get(i).getName());
        }
    }

    @Test
    public void existsById() {
        Assert.assertEquals(0, repository.count());
        @NotNull final Project project = repository.create(userId, projectName);
        Assert.assertTrue(repository.existsById(project.getId()));
        Assert.assertFalse(repository.existsById(UUID.randomUUID().toString()));
        Assert.assertFalse(repository.existsById(null, project.getId()));
        Assert.assertFalse(repository.existsById("", project.getId()));
        Assert.assertFalse(repository.existsById(UUID.randomUUID().toString(), project.getId()));
        Assert.assertTrue(repository.existsById(userId, project.getId()));
        Assert.assertFalse(repository.existsById(userId, UUID.randomUUID().toString()));
        Assert.assertFalse(repository.existsById(userId, ""));
        Assert.assertFalse(repository.existsById(userId, null));
    }

    @Test
    public void findOneById() {
        Assert.assertEquals(0, repository.count());
        @NotNull final Project project = repository.create(userId, projectName);
        Assert.assertNotNull(repository.findOneById(project.getId()));
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString()));
        Assert.assertNull(repository.findOneById(null, project.getId()));
        Assert.assertNull(repository.findOneById("", project.getId()));
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString(), project.getId()));
        Assert.assertNotNull(repository.findOneById(userId, project.getId()));
        Assert.assertNull(repository.findOneById(userId, UUID.randomUUID().toString()));
        Assert.assertNull(repository.findOneById(userId, ""));
        Assert.assertNull(repository.findOneById(userId, null));
        @Nullable Project projectFind = repository.findOneById(userId, project.getId());
        Assert.assertNotNull(projectFind);
        Assert.assertEquals(project.getId(), projectFind.getId());
    }

    @Test
    public void findOneByIndex() {
        Assert.assertEquals(0, repository.count());
        @NotNull final Project project = repository.create(userId, projectName);
        Assert.assertNotNull(repository.findOneByIndex(0));
        Assert.assertEquals(project.getId(), repository.findOneByIndex(0).getId());
        Assert.assertNotNull(repository.findOneByIndex(userId, 0));
        Assert.assertNull(repository.findOneByIndex(null, 0));
        Assert.assertNull(repository.findOneByIndex("", 0));
        Assert.assertNull(repository.findOneByIndex(UUID.randomUUID().toString(), 0));
    }

    @Test
    public void remove() {
        Assert.assertEquals(0, repository.count());
        @NotNull Project project = repository.create(userId, projectName);
        @Nullable Project projectRemove = repository.remove(project);
        Assert.assertNotNull(projectRemove);
        Assert.assertEquals(0, repository.count());
        Assert.assertEquals(project, projectRemove);
        project = repository.create(userId, projectName);
        repository.create(UUID.randomUUID().toString(), projectName);
        Assert.assertEquals(2, repository.count());
        projectRemove = repository.remove(null, null);
        Assert.assertNull(projectRemove);
        Assert.assertEquals(2, repository.count());
        projectRemove = repository.remove(null, project);
        Assert.assertNull(projectRemove);
        Assert.assertEquals(2, repository.count());
        projectRemove = repository.remove(userId, null);
        Assert.assertNull(projectRemove);
        Assert.assertEquals(2, repository.count());
        projectRemove = repository.remove(UUID.randomUUID().toString(), project);
        Assert.assertNull(projectRemove);
        Assert.assertEquals(2, repository.count());
        projectRemove = repository.remove(userId, project);
        Assert.assertEquals(project, projectRemove);
        Assert.assertEquals(1, repository.count());
    }

    @Test
    public void removeAll() {
        Assert.assertEquals(0, repository.count());
        @NotNull List<Project> projects = new ArrayList<>();
        final int countProject = 10;
        for (int i = 0; i < countProject; i++) {
            @NotNull final String userId = UUID.randomUUID().toString();
            @NotNull final String projectName = UUID.randomUUID().toString();
            @NotNull final Project project = new Project();
            project.setName(projectName);
            project.setUserId(userId);
            projects.add(project);
        }
        repository.add(projects);
        Assert.assertEquals(countProject, repository.count());
        repository.removeAll(null);
        Assert.assertEquals(countProject, repository.count());
        projects.remove(0);
        repository.removeAll(projects);
        Assert.assertEquals(1, repository.count());
    }

    @Test
    public void removeById() {
        Assert.assertEquals(0, repository.count());
        @NotNull Project project = repository.create(userId, projectName);
        @Nullable Project projectRemove = repository.removeById(UUID.randomUUID().toString());
        Assert.assertNull(projectRemove);
        Assert.assertEquals(1, repository.count());
        projectRemove = repository.removeById(project.getId());
        Assert.assertEquals(0, repository.count());
        Assert.assertEquals(project, projectRemove);
        project = repository.create(userId, projectName);
        repository.create(UUID.randomUUID().toString(), projectName);
        Assert.assertEquals(2, repository.count());
        projectRemove = repository.removeById(UUID.randomUUID().toString(), project.getId());
        Assert.assertNull(projectRemove);
        Assert.assertEquals(2, repository.count());
        projectRemove = repository.removeById(UUID.randomUUID().toString(), UUID.randomUUID().toString());
        Assert.assertNull(projectRemove);
        Assert.assertEquals(2, repository.count());
        projectRemove = repository.removeById(userId, UUID.randomUUID().toString());
        Assert.assertNull(projectRemove);
        Assert.assertEquals(2, repository.count());
        projectRemove = repository.removeById(userId, project.getId());
        Assert.assertEquals(project, projectRemove);
        Assert.assertEquals(1, repository.count());
    }

    @Test
    public void removeByIndex() {
        Assert.assertEquals(0, repository.count());
        @NotNull Project project = repository.create(userId, projectName);
        @Nullable Project projectRemove = repository.removeByIndex(0);
        Assert.assertNotNull(projectRemove);
        Assert.assertEquals(0, repository.count());
        Assert.assertEquals(project, projectRemove);
    }

    @Test
    public void count() {
        Assert.assertEquals(0, repository.count());
        final int countProject = 10;
        for (int i = 0; i < countProject; i++) {
            @NotNull final String userId = UUID.randomUUID().toString();
            @NotNull final String projectName = UUID.randomUUID().toString();
            repository.create(userId, projectName);
            Assert.assertEquals(i + 1, repository.count());
            Assert.assertEquals(repository.count(), repository.findAll().size());
        }
    }

    @Test
    public void findAllId() {
        @NotNull Project project_0 = repository.create(userId, projectName, projectDescription);
        @NotNull Project project_1 = repository.create(userId, projectName, projectDescription);
        Assert.assertEquals(0, repository.findAllId("").length);
        Assert.assertEquals(0, repository.findAllId(null).length);
        Assert.assertEquals(0, repository.findAllId(UUID.randomUUID().toString()).length);
        @NotNull String[] listId = repository.findAllId(userId);
        Assert.assertEquals(2, listId.length);
        Assert.assertEquals(project_0.getId(), listId[0]);
        Assert.assertEquals(project_1.getId(), listId[1]);
    }

}
