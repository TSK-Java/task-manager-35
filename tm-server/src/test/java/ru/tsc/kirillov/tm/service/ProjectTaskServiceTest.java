package ru.tsc.kirillov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import ru.tsc.kirillov.tm.api.repository.IProjectRepository;
import ru.tsc.kirillov.tm.api.repository.ITaskRepository;
import ru.tsc.kirillov.tm.api.repository.IUserRepository;
import ru.tsc.kirillov.tm.api.service.*;
import ru.tsc.kirillov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kirillov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kirillov.tm.exception.field.ProjectIdEmptyException;
import ru.tsc.kirillov.tm.exception.field.TaskIdEmptyException;
import ru.tsc.kirillov.tm.exception.field.UserIdEmptyException;
import ru.tsc.kirillov.tm.model.Project;
import ru.tsc.kirillov.tm.model.Task;
import ru.tsc.kirillov.tm.model.User;
import ru.tsc.kirillov.tm.repository.ProjectRepository;
import ru.tsc.kirillov.tm.repository.TaskRepository;
import ru.tsc.kirillov.tm.repository.UserRepository;

import java.util.UUID;

public final class ProjectTaskServiceTest {

    @NotNull final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull final ITaskRepository taskRepository = new TaskRepository();

    @NotNull final IUserRepository userRepository = new UserRepository();

    @NotNull final IPropertyService propertyService = new PropertyService();

    @NotNull final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull final ITaskService taskService = new TaskService(taskRepository);

    @NotNull final IProjectTaskService projectTaskService = new ProjectTaskService(projectService, taskService);

    @NotNull
    private final IUserService userService = new UserService(
            propertyService,
            userRepository,
            taskRepository,
            projectRepository
    );

    @NotNull final String projectName = UUID.randomUUID().toString();

    @NotNull final String taskName = UUID.randomUUID().toString();

    @NotNull final String userLogin = UUID.randomUUID().toString();

    @NotNull final String userPassword = UUID.randomUUID().toString();

    @After
    public void finalization() {
        projectRepository.clear();
        taskRepository.clear();
        userService.clear();
    }

    @Test
    public void bindTaskToProject() {
        @NotNull User user = userService.create(userLogin, userPassword);
        @NotNull String userId = user.getId();
        @NotNull Project project = projectRepository.create(userId, projectName);
        @NotNull Task task = taskRepository.create(userId, taskName);
        @NotNull Project projectRandom = projectRepository.create(UUID.randomUUID().toString(), projectName);
        @NotNull Task taskRandom = taskRepository.create(UUID.randomUUID().toString(), taskName);
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject("", "", "")
        );
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(null, null, null)
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(userId, "", "")
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(userId, null, null)
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(userId, project.getId(), "")
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(userId, project.getId(), null)
        );
        Assert.assertThrows(
                ProjectNotFoundException.class,
                () -> projectTaskService.bindTaskToProject(userId, projectRandom.getId(), taskRandom.getId())
        );
        Assert.assertThrows(
                TaskNotFoundException.class,
                () -> projectTaskService.bindTaskToProject(userId, project.getId(), taskRandom.getId())
        );
        Assert.assertNull(task.getProjectId());
        projectTaskService.bindTaskToProject(userId, project.getId(), task.getId());
        Assert.assertNotNull(task.getProjectId());
        Assert.assertEquals(project.getId(), task.getProjectId());
    }

    @Test
    public void unbindTaskToProject() {
        @NotNull User user = userService.create(userLogin, userPassword);
        @NotNull String userId = user.getId();
        @NotNull Project project = projectRepository.create(userId, projectName);
        @NotNull Task task = taskRepository.create(userId, taskName);
        @NotNull Project projectRandom = projectRepository.create(UUID.randomUUID().toString(), projectName);
        @NotNull Task taskRandom = taskRepository.create(UUID.randomUUID().toString(), taskName);
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.unbindTaskToProject("", "", "")
        );
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.unbindTaskToProject(null, null, null)
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.unbindTaskToProject(userId, "", "")
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.unbindTaskToProject(userId, null, null)
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> projectTaskService.unbindTaskToProject(userId, project.getId(), "")
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> projectTaskService.unbindTaskToProject(userId, project.getId(), null)
        );
        Assert.assertThrows(
                ProjectNotFoundException.class,
                () -> projectTaskService.unbindTaskToProject(userId, projectRandom.getId(), taskRandom.getId())
        );
        Assert.assertThrows(
                TaskNotFoundException.class,
                () -> projectTaskService.unbindTaskToProject(userId, project.getId(), taskRandom.getId())
        );
        task.setProjectId(project.getId());
        Assert.assertNotNull(task.getProjectId());
        Assert.assertEquals(project.getId(), task.getProjectId());
        projectTaskService.unbindTaskToProject(userId, project.getId(), task.getId());
        Assert.assertNull(task.getProjectId());
    }

    @Test
    public void removeProjectById() {
        @NotNull User user = userService.create(userLogin, userPassword);
        @NotNull String userId = user.getId();
        @NotNull Project project = projectRepository.create(userId, projectName);
        @NotNull Task task = taskRepository.create(userId, taskName);
        projectRepository.create(UUID.randomUUID().toString(), projectName);
        taskRepository.create(UUID.randomUUID().toString(), taskName);
        task.setProjectId(project.getId());
        Assert.assertNotNull(task.getProjectId());
        Assert.assertEquals(project.getId(), task.getProjectId());
        Assert.assertEquals(2, taskRepository.count());
        Assert.assertEquals(2, projectRepository.count());
        Assert.assertEquals(1, taskRepository.count(userId));
        Assert.assertEquals(1, projectRepository.count(userId));
        Assert.assertThrows(
                UserIdEmptyException.class, () -> projectTaskService.removeProjectById("", "")
        );
        Assert.assertThrows(
                UserIdEmptyException.class, () -> projectTaskService.removeProjectById(null, null)
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class, () -> projectTaskService.removeProjectById(userId, "")
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class, () -> projectTaskService.removeProjectById(userId, null)
        );
        projectTaskService.removeProjectById(userId, project.getId());
        Assert.assertEquals(1, taskRepository.count());
        Assert.assertEquals(1, projectRepository.count());
        Assert.assertEquals(0, taskRepository.count(userId));
        Assert.assertEquals(0, projectRepository.count(userId));
    }

    @Test
    public void removeProjectByIndex() {
        @NotNull User user = userService.create(userLogin, userPassword);
        @NotNull String userId = user.getId();
        @NotNull Project project = projectRepository.create(userId, projectName);
        @NotNull Task task = taskRepository.create(userId, taskName);
        projectRepository.create(UUID.randomUUID().toString(), projectName);
        taskRepository.create(UUID.randomUUID().toString(), taskName);
        task.setProjectId(project.getId());
        Assert.assertNotNull(task.getProjectId());
        Assert.assertEquals(project.getId(), task.getProjectId());
        Assert.assertEquals(2, taskRepository.count());
        Assert.assertEquals(2, projectRepository.count());
        Assert.assertEquals(1, taskRepository.count(userId));
        Assert.assertEquals(1, projectRepository.count(userId));
        Assert.assertThrows(
                UserIdEmptyException.class, () -> projectTaskService.removeProjectByIndex("", 1)
        );
        Assert.assertThrows(
                UserIdEmptyException.class, () -> projectTaskService.removeProjectByIndex(null, null)
        );
        Assert.assertThrows(
                IndexOutOfBoundsException.class, () -> projectTaskService.removeProjectByIndex(userId, null)
        );
        Assert.assertThrows(
                IndexOutOfBoundsException.class,
                () -> projectTaskService.removeProjectByIndex(UUID.randomUUID().toString(), 1)
        );
        Assert.assertThrows(
                IndexOutOfBoundsException.class, () -> projectTaskService.removeProjectByIndex(userId, -50)
        );
        Assert.assertThrows(
                IndexOutOfBoundsException.class, () -> projectTaskService.removeProjectByIndex(userId, 50)
        );
        projectTaskService.removeProjectByIndex(userId, 1);
        Assert.assertEquals(1, taskRepository.count());
        Assert.assertEquals(1, projectRepository.count());
        Assert.assertEquals(0, taskRepository.count(userId));
        Assert.assertEquals(0, projectRepository.count(userId));
    }

    @Test
    public void clear() {
        @NotNull User user = userService.create(userLogin, userPassword);
        @NotNull String userId = user.getId();
        @NotNull Project project = projectRepository.create(userId, projectName);
        @NotNull Task task = taskRepository.create(userId, taskName);
        projectRepository.create(UUID.randomUUID().toString(), projectName);
        taskRepository.create(UUID.randomUUID().toString(), taskName);
        task.setProjectId(project.getId());
        Assert.assertNotNull(task.getProjectId());
        Assert.assertEquals(project.getId(), task.getProjectId());
        Assert.assertEquals(2, taskRepository.count());
        Assert.assertEquals(2, projectRepository.count());
        Assert.assertEquals(1, taskRepository.count(userId));
        Assert.assertEquals(1, projectRepository.count(userId));
        Assert.assertThrows(
                UserIdEmptyException.class, () -> projectTaskService.clear("")
        );
        Assert.assertThrows(
                UserIdEmptyException.class, () -> projectTaskService.clear(null)
        );
        Assert.assertThrows(
                ProjectNotFoundException.class,
                () -> projectTaskService.clear(UUID.randomUUID().toString())
        );
        projectTaskService.clear(userId);
        Assert.assertEquals(1, taskRepository.count());
        Assert.assertEquals(1, projectRepository.count());
        Assert.assertEquals(0, taskRepository.count(userId));
        Assert.assertEquals(0, projectRepository.count(userId));
    }

}
