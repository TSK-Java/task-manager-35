package ru.tsc.kirillov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import ru.tsc.kirillov.tm.api.repository.IProjectRepository;
import ru.tsc.kirillov.tm.api.service.IProjectService;
import ru.tsc.kirillov.tm.enumerated.Sort;
import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kirillov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kirillov.tm.exception.field.*;
import ru.tsc.kirillov.tm.exception.system.IndexOutOfBoundsException;
import ru.tsc.kirillov.tm.model.Project;
import ru.tsc.kirillov.tm.repository.ProjectRepository;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public final class ProjectServiceTest {

    @NotNull final IProjectRepository repository = new ProjectRepository();

    @NotNull final IProjectService service = new ProjectService(repository);

    @NotNull final String userId = UUID.randomUUID().toString();

    @NotNull final String projectName = UUID.randomUUID().toString();

    @NotNull final String projectDescription = UUID.randomUUID().toString();

    @NotNull final Date dateBegin = new Date();

    @NotNull final Date dateEnd = new Date();

    @NotNull final String projectNewName = UUID.randomUUID().toString();

    @NotNull final String projectNewDescription = UUID.randomUUID().toString();

    @NotNull final Status projectStatus = Status.COMPLETED;

    @After
    public void finalization() {
        service.clear();
    }

    @Test
    public void add() {
        Assert.assertEquals(0, service.count());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.add(null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.add("", null));
        Assert.assertNull(service.add(userId, null));
        @NotNull final Project project = new Project();
        @Nullable Project projectAdd = service.add(userId, project);
        Assert.assertNotNull(projectAdd);
        Assert.assertEquals(project, projectAdd);
        Assert.assertEquals(1, service.count());
        Assert.assertEquals(1, service.count(userId));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.count(""));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.count(null));
        Assert.assertEquals(0, service.count(UUID.randomUUID().toString()));
    }

    @Test
    public void clear() {
        Assert.assertEquals(0, service.count());
        service.create(userId, projectName);
        Assert.assertEquals(1, service.count());
        service.clear();
        Assert.assertEquals(0, service.count());
        service.create(userId, projectName);
        service.create(UUID.randomUUID().toString(), projectName);
        Assert.assertEquals(1, service.count(userId));
        Assert.assertEquals(0, service.count(UUID.randomUUID().toString()));
        service.clear(userId);
        Assert.assertEquals(1, service.count());
        Assert.assertEquals(0, service.count(userId));
        Assert.assertEquals(0, service.count(UUID.randomUUID().toString()));
    }

    @Test
    public void findAll() {
        Assert.assertEquals(0, service.count());
        service.create(userId, projectName);
        Assert.assertEquals(1, service.count());
        Assert.assertEquals(service.count(), service.findAll().size());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(""));
        @Nullable final String userIdNull = null;
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(userIdNull));
        @NotNull List<Project> projects = service.findAll(userId);
        Assert.assertEquals(service.count(), projects.size());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(userIdNull, Sort.BY_NAME.getComparator()).size());
        service.clear();
        final int countProject = 10;
        for (int i = 0; i < countProject; i++) {
            @NotNull final String projectName = String.format("Project_%d", countProject - i - 1);
            service.create(userId, projectName);
        }
        projects = service.findAll(userId, Sort.BY_NAME.getComparator());
        Assert.assertEquals(countProject, projects.size());
        for (int i = 0; i < countProject; i++) {
            @NotNull final String projectName = String.format("Project_%d", i);
            Assert.assertEquals(projectName, projects.get(i).getName());
        }
        projects = service.findAll(userId, Sort.BY_NAME);
        Assert.assertEquals(countProject, projects.size());
        for (int i = 0; i < countProject; i++) {
            @NotNull final String projectName = String.format("Project_%d", i);
            Assert.assertEquals(projectName, projects.get(i).getName());
        }
    }

    @Test
    public void existsById() {
        Assert.assertEquals(0, service.count());
        @NotNull final Project project = service.create(userId, projectName);
        Assert.assertTrue(service.existsById(project.getId()));
        Assert.assertFalse(service.existsById(UUID.randomUUID().toString()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existsById(null, project.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existsById("", project.getId()));
        Assert.assertFalse(service.existsById(UUID.randomUUID().toString(), project.getId()));
        Assert.assertTrue(service.existsById(userId, project.getId()));
        Assert.assertFalse(service.existsById(userId, UUID.randomUUID().toString()));
        Assert.assertThrows(IdEmptyException.class, () -> service.existsById(userId, ""));
        Assert.assertThrows(IdEmptyException.class, () -> service.existsById(userId, null));
    }

    @Test
    public void findOneById() {
        Assert.assertEquals(0, service.count());
        @NotNull final Project project = service.create(userId, projectName);
        Assert.assertNotNull(service.findOneById(project.getId()));
        Assert.assertNull(service.findOneById(UUID.randomUUID().toString()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneById(null, project.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneById("", project.getId()));
        Assert.assertNull(service.findOneById(UUID.randomUUID().toString(), project.getId()));
        Assert.assertNotNull(service.findOneById(userId, project.getId()));
        Assert.assertNull(service.findOneById(userId, UUID.randomUUID().toString()));
        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(userId, ""));
        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(userId, null));
        @Nullable Project projectFind = service.findOneById(userId, project.getId());
        Assert.assertNotNull(projectFind);
        Assert.assertEquals(project.getId(), projectFind.getId());
    }

    @Test
    public void findOneByIndex() {
        Assert.assertEquals(0, service.count());
        @NotNull final Project project = service.create(userId, projectName);
        @Nullable Project projectFind = service.findOneByIndex(0);
        Assert.assertNotNull(projectFind);
        Assert.assertEquals(project, projectFind);
        Assert.assertEquals(project.getId(), projectFind.getId());
        projectFind = service.findOneByIndex(userId,0);
        Assert.assertNotNull(projectFind);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneByIndex(null, 0));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneByIndex("", 0));
        Assert.assertThrows(IndexOutOfBoundsException.class, () -> service.findOneByIndex(userId, -50));
        Assert.assertThrows(IndexOutOfBoundsException.class, () -> service.findOneByIndex(userId, 50));
        Assert.assertThrows(IndexOutOfBoundsException.class, () -> service.findOneByIndex(UUID.randomUUID().toString(), 0));
    }

    @Test
    public void remove() {
        Assert.assertEquals(0, service.count());
        @NotNull Project project = service.create(userId, projectName);
        @Nullable Project projectRemove = service.remove(project);
        Assert.assertNotNull(projectRemove);
        Assert.assertEquals(0, service.count());
        Assert.assertEquals(project, projectRemove);
        project = service.create(userId, projectName);
        service.create(UUID.randomUUID().toString(), projectName);
        Assert.assertEquals(2, service.count());
        Assert.assertEquals(1, service.count(userId));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.remove(null, null));
        @NotNull final Project projectFinal = project;
        Assert.assertThrows(UserIdEmptyException.class, () -> service.remove(null, projectFinal));
        Assert.assertNull(service.remove(userId, null));
        Assert.assertEquals(2, service.count());
        Assert.assertNull(service.remove(UUID.randomUUID().toString(), project));
        Assert.assertEquals(2, service.count());
        Assert.assertEquals(project, service.remove(userId, project));
        Assert.assertEquals(1, service.count());
        Assert.assertEquals(0, service.count(userId));
    }

    @Test
    public void removeById() {
        Assert.assertEquals(0, service.count());
        @NotNull Project project = service.create(userId, projectName);
        Assert.assertThrows(EntityNotFoundException.class, () -> service.removeById(UUID.randomUUID().toString()));
        Assert.assertEquals(1, service.count());
        Assert.assertEquals(project, service.removeById(project.getId()));
        Assert.assertEquals(0, service.count());
        project = service.create(userId, projectName);
        service.create(UUID.randomUUID().toString(), projectName);
        Assert.assertEquals(2, service.count());
        Assert.assertEquals(1, service.count(userId));
        Assert.assertNull(service.removeById(UUID.randomUUID().toString(), project.getId()));
        Assert.assertEquals(2, service.count());
        Assert.assertNull(service.removeById(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
        Assert.assertEquals(2, service.count());
        Assert.assertNull(service.removeById(userId, UUID.randomUUID().toString()));
        Assert.assertEquals(2, service.count());
        @NotNull final Project projectFinal = project;
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeById(null, projectFinal.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeById("", projectFinal.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(userId, null));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(userId, ""));
        Assert.assertEquals(project, service.removeById(userId, project.getId()));
        Assert.assertEquals(1, service.count());
        Assert.assertEquals(0, service.count(userId));
    }

    @Test
    public void removeByIndex() {
        Assert.assertEquals(0, service.count());
        @NotNull Project project = service.create(userId, projectName);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeByIndex(null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeByIndex("", null));
        Assert.assertThrows(IndexOutOfBoundsException.class, () -> service.removeByIndex(userId, null));
        Assert.assertThrows(IndexOutOfBoundsException.class, () -> service.removeByIndex(userId, -50));
        Assert.assertThrows(IndexOutOfBoundsException.class, () -> service.removeByIndex(userId, 50));
        Assert.assertThrows(IndexOutOfBoundsException.class, () -> service.removeByIndex(UUID.randomUUID().toString(), 0));
        @Nullable Project projectRemove = service.removeByIndex(userId, 0);
        Assert.assertNotNull(projectRemove);
        Assert.assertEquals(0, service.count());
        Assert.assertEquals(project, projectRemove);
    }

    @Test
    public void count() {
        Assert.assertEquals(0, service.count());
        final int countProject = 10;
        for (int i = 0; i < countProject; i++) {
            @NotNull final String userIdRandom = UUID.randomUUID().toString();
            @NotNull final String projectName = UUID.randomUUID().toString();
            service.create(userId, projectName);
            service.create(userIdRandom, projectName);
            Assert.assertEquals(i*2 + 2, service.count());
            long cnt = service.count(userId);
            Assert.assertEquals(i + 1, cnt);
            Assert.assertEquals(service.count(), service.findAll().size());
            Assert.assertEquals(service.count(userId), service.findAll(userId).size());
        }
    }

    @Test
    public void create() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.create("", ""));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(userId, ""));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.create(null, null));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(userId, null));
        Assert.assertEquals(0, service.count());
        @NotNull Project project = service.create(userId, projectName);
        Assert.assertNotNull(project);
        Assert.assertEquals(1, service.count());
        @Nullable Project projectFind = service.findOneById(userId, project.getId());
        Assert.assertNotNull(projectFind);
        Assert.assertEquals(project, projectFind);
        Assert.assertEquals(project.getName(), projectFind.getName());
    }

    @Test
    public void createDescription() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.create("", "", ""));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(userId, "", ""));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.create(userId, projectName, ""));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.create(null, null, null));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(userId, null, null));
        Assert.assertEquals(0, service.count());
        @NotNull Project project = service.create(userId, projectName, projectDescription);
        Assert.assertNotNull(project);
        Assert.assertEquals(1, service.count());
        @Nullable Project projectFind = service.findOneById(userId, project.getId());
        Assert.assertNotNull(projectFind);
        Assert.assertEquals(project, projectFind);
        Assert.assertEquals(project.getName(), projectFind.getName());
        Assert.assertEquals(project.getDescription(), projectFind.getDescription());
    }

    @Test
    public void createDate() {
        Assert.assertEquals(0, service.count());
        @NotNull Project project = service.create(userId, projectName, projectDescription, dateBegin, dateEnd);
        Assert.assertNotNull(project);
        Assert.assertEquals(1, service.count());
        @Nullable Project projectFind = service.findOneById(userId, project.getId());
        Assert.assertNotNull(projectFind);
        Assert.assertEquals(project, projectFind);
        Assert.assertEquals(projectName, projectFind.getName());
        Assert.assertEquals(projectDescription, projectFind.getDescription());
        Assert.assertEquals(dateBegin, projectFind.getDateBegin());
        Assert.assertEquals(dateEnd, projectFind.getDateEnd());
    }

    @Test
    public void updateById() {
        @NotNull Project project = service.create(userId, projectName, projectDescription, dateBegin, dateEnd);
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.updateById("", "", "", "")
        );
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.updateById(null, null, null, null)
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> service.updateById(userId, "", "", "")
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> service.updateById(userId, null, null, null)
        );
        Assert.assertThrows(
                NameEmptyException.class,
                () -> service.updateById(userId, project.getId(), "", "")
        );
        Assert.assertThrows(
                NameEmptyException.class,
                () -> service.updateById(userId, project.getId(), null, null)
        );
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> service.updateById(userId, project.getId(), projectNewName, "")
        );
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> service.updateById(userId, project.getId(), projectNewName, null)
        );
        Assert.assertThrows(
                ProjectNotFoundException.class,
                () -> service.updateById(userId, UUID.randomUUID().toString(), projectNewName, projectNewDescription)
        );
        Assert.assertThrows(
                ProjectNotFoundException.class,
                () -> service.updateById(
                        UUID.randomUUID().toString(), project.getId(), projectNewName, projectNewDescription
                )
        );
        @NotNull Project projectUpdated =
                service.updateById(userId, project.getId(), projectNewName, projectNewDescription);
        Assert.assertNotNull(projectUpdated);
        Assert.assertEquals(project, projectUpdated);
        Assert.assertEquals(projectNewName, projectUpdated.getName());
        Assert.assertEquals(projectNewDescription, projectUpdated.getDescription());
    }

    @Test
    public void updateByIndex() {
        @NotNull Project project = service.create(userId, projectName, projectDescription, dateBegin, dateEnd);
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.updateByIndex("", null, "", "")
        );
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.updateByIndex(null, null, null, null)
        );
        Assert.assertThrows(
                IndexOutOfBoundsException.class,
                () -> service.updateByIndex(userId, null, "", "")
        );
        Assert.assertThrows(
                IndexOutOfBoundsException.class,
                () -> service.updateByIndex(userId, null, null, null)
        );
        Assert.assertThrows(
                IndexOutOfBoundsException.class,
                () -> service.updateByIndex(userId, -50, "", "")
        );
        Assert.assertThrows(
                IndexOutOfBoundsException.class,
                () -> service.updateByIndex(userId, -50, null, null)
        );
        Assert.assertThrows(
                IndexOutOfBoundsException.class,
                () -> service.updateByIndex(userId, 50, "", "")
        );
        Assert.assertThrows(
                IndexOutOfBoundsException.class,
                () -> service.updateByIndex(userId, 50, null, null)
        );
        Assert.assertThrows(
                NameEmptyException.class,
                () -> service.updateByIndex(userId, 0, "", "")
        );
        Assert.assertThrows(
                NameEmptyException.class,
                () -> service.updateByIndex(userId, 0, null, null)
        );
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> service.updateByIndex(userId, 0, projectNewName, "")
        );
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> service.updateByIndex(userId, 0, projectNewName, null)
        );
        Assert.assertThrows(
                IndexOutOfBoundsException.class,
                () -> service.updateByIndex(UUID.randomUUID().toString(), 0, projectNewName, projectNewDescription)
        );
        @NotNull Project projectUpdated = service.updateByIndex(userId, 0, projectNewName, projectNewDescription);
        Assert.assertNotNull(projectUpdated);
        Assert.assertEquals(project, projectUpdated);
        Assert.assertEquals(projectNewName, projectUpdated.getName());
        Assert.assertEquals(projectNewDescription, projectUpdated.getDescription());
    }

    @Test
    public void changeProjectStatusByIdAllEmpty() {
        @NotNull Project project = service.create(userId, projectName, projectDescription, dateBegin, dateEnd);
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.changeProjectStatusById("", "", null)
        );
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.changeProjectStatusById(null, null, null)
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> service.changeProjectStatusById(userId, "", null)
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> service.changeProjectStatusById(userId, null, null)
        );
        Assert.assertThrows(
                StatusEmptyException.class,
                () -> service.changeProjectStatusById(userId, project.getId(), null)
        );
        Assert.assertThrows(
                ProjectNotFoundException.class,
                () -> service.changeProjectStatusById(userId, UUID.randomUUID().toString(), projectStatus)
        );
        Assert.assertThrows(
                ProjectNotFoundException.class,
                () -> service.changeProjectStatusById(UUID.randomUUID().toString(), project.getId(), projectStatus)
        );
        @NotNull Project projectChanged = service.changeProjectStatusById(userId, project.getId(), projectStatus);
        Assert.assertNotNull(projectChanged);
        Assert.assertEquals(project, projectChanged);
        Assert.assertEquals(projectStatus, projectChanged.getStatus());
    }

    @Test
    public void changeProjectStatusByIndexAllEmpty() {
        @NotNull Project project = service.create(userId, projectName, projectDescription, dateBegin, dateEnd);
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.changeProjectStatusByIndex("", null, null)
        );
        Assert.assertThrows(
                IndexOutOfBoundsException.class,
                () -> service.changeProjectStatusByIndex(userId, null, null)
        );
        Assert.assertThrows(
                IndexOutOfBoundsException.class,
                () -> service.changeProjectStatusByIndex(userId, -50, null)
        );
        Assert.assertThrows(
                IndexOutOfBoundsException.class,
                () -> service.changeProjectStatusByIndex(userId, 50, null)
        );
        Assert.assertThrows(
                StatusEmptyException.class,
                () -> service.changeProjectStatusByIndex(userId, 0, null)
        );
        Assert.assertThrows(
                IndexOutOfBoundsException.class,
                () -> service.changeProjectStatusByIndex(UUID.randomUUID().toString(), 0, null)
        );
        @NotNull Project projectChanged = service.changeProjectStatusByIndex(userId,0, projectStatus);
        Assert.assertNotNull(projectChanged);
        Assert.assertEquals(project, projectChanged);
        Assert.assertEquals(projectStatus, projectChanged.getStatus());
    }

    @Test
    public void findAllId() {
        @NotNull Project project_0 = service.create(userId, projectName, projectDescription, dateBegin, dateEnd);
        @NotNull Project project_1 = service.create(userId, projectName, projectDescription, dateBegin, dateEnd);
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.findAllId("")
        );
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.findAllId(null)
        );
        Assert.assertEquals(0, service.findAllId(UUID.randomUUID().toString()).length);
        @NotNull String[] listId = service.findAllId(userId);
        Assert.assertEquals(2, listId.length);
        Assert.assertEquals(project_0.getId(), listId[0]);
        Assert.assertEquals(project_1.getId(), listId[1]);
    }

}
