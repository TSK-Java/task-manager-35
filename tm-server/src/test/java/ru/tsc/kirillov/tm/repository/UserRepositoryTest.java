package ru.tsc.kirillov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import ru.tsc.kirillov.tm.api.repository.IUserRepository;
import ru.tsc.kirillov.tm.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public final class UserRepositoryTest {

    @NotNull final IUserRepository repository = new UserRepository();

    @NotNull final String userLogin = UUID.randomUUID().toString();

    @NotNull final String userPassword = UUID.randomUUID().toString();

    @After
    public void finalization() {
        repository.clear();
    }

    @Test
    public void add() {
        Assert.assertEquals(0, repository.count());
        @NotNull final User user = new User();
        user.setLogin(userLogin);
        user.setPasswordHash(userPassword);
        repository.add(user);
        Assert.assertEquals(1, repository.count());
        @Nullable User userfind = repository.findOneById(user.getId());
        Assert.assertNotNull(userfind);
        Assert.assertEquals(user, userfind);
        Assert.assertEquals(user.getLogin(), userfind.getLogin());
    }
    
    @Test
    public void addList() {
        Assert.assertEquals(0, repository.count());
        @NotNull List<User> users = new ArrayList<>();
        final int countUser = 10;
        for (int i = 0; i < countUser; i++) {
            @NotNull final String userLogin = UUID.randomUUID().toString();
            @NotNull final String userPassword = UUID.randomUUID().toString();
            @NotNull final User user = new User();
            user.setLogin(userLogin);
            user.setPasswordHash(userPassword);
            users.add(user);
        }
        repository.add(users);
        Assert.assertEquals(countUser, repository.count());
        for (int i = 0; i < countUser; i++) {
            @NotNull final User user = users.get(i);
            @Nullable final User userFind = repository.findOneById(user.getId());
            Assert.assertNotNull(userFind);
            Assert.assertEquals(users.get(i).getLogin(), userFind.getLogin());
        }
    }
    
    @Test
    public void clear() {
        Assert.assertEquals(0, repository.count());
        @NotNull final User user = new User();
        user.setLogin(userLogin);
        user.setPasswordHash(userPassword);
        repository.add(user);
        Assert.assertEquals(1, repository.count());
        repository.clear();
        Assert.assertEquals(0, repository.count());
    }

    @Test
    public void findAll() {
        @NotNull final User user = new User();
        user.setLogin(userLogin);
        user.setPasswordHash(userPassword);
        repository.add(user);
        Assert.assertEquals(1, repository.findAll().size());
    }

    @Test
    public void existsById() {
        Assert.assertEquals(0, repository.count());
        @NotNull final User user = new User();
        user.setLogin(userLogin);
        user.setPasswordHash(userPassword);
        repository.add(user);
        Assert.assertTrue(repository.existsById(user.getId()));
        Assert.assertFalse(repository.existsById(UUID.randomUUID().toString()));
        Assert.assertFalse(repository.existsById(""));
    }

    @Test
    public void findOneById() {
        Assert.assertEquals(0, repository.count());
        @NotNull final User user = new User();
        user.setLogin(userLogin);
        user.setPasswordHash(userPassword);
        repository.add(user);
        Assert.assertNotNull(repository.findOneById(user.getId()));
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString()));
        Assert.assertNull(repository.findOneById(""));
    }

    @Test
    public void findOneByIndex() {
        Assert.assertEquals(0, repository.count());
        @NotNull final User user = new User();
        user.setLogin(userLogin);
        user.setPasswordHash(userPassword);
        repository.add(user);
        Assert.assertNotNull(repository.findOneByIndex(0));
        Assert.assertEquals(user.getId(), repository.findOneByIndex(0).getId());
    }

    @Test
    public void remove() {
        Assert.assertEquals(0, repository.count());
        @NotNull final User user = new User();
        user.setLogin(userLogin);
        user.setPasswordHash(userPassword);
        repository.add(user);
        @Nullable User userRemove = repository.remove(user);
        Assert.assertEquals(0, repository.count());
        Assert.assertEquals(user, userRemove);
    }

    @Test
    public void removeAll() {
        Assert.assertEquals(0, repository.count());
        @NotNull List<User> users = new ArrayList<>();
        final int countUser = 10;
        for (int i = 0; i < countUser; i++) {
            @NotNull final String userLogin = UUID.randomUUID().toString();
            @NotNull final String userPassword = UUID.randomUUID().toString();
            @NotNull final User user = new User();
            user.setLogin(userLogin);
            user.setPasswordHash(userPassword);
            users.add(user);
        }
        repository.add(users);
        Assert.assertEquals(countUser, repository.count());
        repository.removeAll(null);
        Assert.assertEquals(countUser, repository.count());
        users.remove(0);
        repository.removeAll(users);
        Assert.assertEquals(1, repository.count());
    }

    @Test
    public void removeById() {
        Assert.assertEquals(0, repository.count());
        @NotNull final User user = new User();
        user.setLogin(userLogin);
        user.setPasswordHash(userPassword);
        repository.add(user);
        @Nullable User userRemove = repository.removeById(UUID.randomUUID().toString());
        Assert.assertNull(userRemove);
        Assert.assertEquals(1, repository.count());
        userRemove = repository.removeById(user.getId());
        Assert.assertEquals(0, repository.count());
        Assert.assertEquals(user, userRemove);
    }

    @Test
    public void removeByIndex() {
        Assert.assertEquals(0, repository.count());
        @NotNull final User user = new User();
        user.setLogin(userLogin);
        user.setPasswordHash(userPassword);
        repository.add(user);
        @Nullable User userRemove = repository.removeByIndex(0);
        Assert.assertNotNull(userRemove);
        Assert.assertEquals(0, repository.count());
        Assert.assertEquals(user, userRemove);
    }

    @Test
    public void count() {
        Assert.assertEquals(0, repository.count());
        final int countUser = 10;
        for (int i = 0; i < countUser; i++) {
            @NotNull final String userLogin = UUID.randomUUID().toString();
            @NotNull final String userPassword = UUID.randomUUID().toString();
            @NotNull final User user = new User();
            user.setLogin(userLogin);
            user.setPasswordHash(userPassword);
            repository.add(user);
            Assert.assertEquals(i + 1, repository.count());
            Assert.assertEquals(repository.count(), repository.findAll().size());
        }
    }

}
