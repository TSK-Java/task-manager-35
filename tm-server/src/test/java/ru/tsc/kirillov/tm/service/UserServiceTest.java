package ru.tsc.kirillov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import ru.tsc.kirillov.tm.api.repository.IProjectRepository;
import ru.tsc.kirillov.tm.api.repository.ITaskRepository;
import ru.tsc.kirillov.tm.api.repository.IUserRepository;
import ru.tsc.kirillov.tm.api.service.IProjectService;
import ru.tsc.kirillov.tm.api.service.IPropertyService;
import ru.tsc.kirillov.tm.api.service.ITaskService;
import ru.tsc.kirillov.tm.api.service.IUserService;
import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.exception.field.*;
import ru.tsc.kirillov.tm.exception.user.UserNameAlreadyExistsException;
import ru.tsc.kirillov.tm.exception.user.UserNotFoundException;
import ru.tsc.kirillov.tm.model.User;
import ru.tsc.kirillov.tm.repository.ProjectRepository;
import ru.tsc.kirillov.tm.repository.TaskRepository;
import ru.tsc.kirillov.tm.repository.UserRepository;
import ru.tsc.kirillov.tm.util.HashUtil;

import java.util.UUID;

public final class UserServiceTest {

    @NotNull final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull final IUserRepository userRepository = new UserRepository();
    
    @NotNull final ITaskRepository taskRepository = new TaskRepository();

    @NotNull final IPropertyService propertyService = new PropertyService();

    @NotNull final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IUserService userService = new UserService(
            propertyService,
            userRepository,
            taskRepository,
            projectRepository
    );

    @NotNull final String userLogin = UUID.randomUUID().toString();

    @NotNull final String userPassword = UUID.randomUUID().toString();

    @NotNull final String userEmail = UUID.randomUUID().toString();

    @NotNull final String projectName = UUID.randomUUID().toString();

    @NotNull final String taskName = UUID.randomUUID().toString();

    @After
    public void finalization() {
        userService.clear();
        projectService.clear();
        taskService.clear();
    }

    @Test
    public void create() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create("", ""));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(null, null));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create(userLogin, ""));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create(userLogin, null));
        Assert.assertEquals(0, userService.count());
        @NotNull User user = userService.create(userLogin, userPassword);
        Assert.assertNotNull(user);
        Assert.assertEquals(1, userService.count());
        @Nullable User userFind = userService.findOneById(user.getId());
        Assert.assertNotNull(userFind);
        Assert.assertEquals(user, userFind);
        Assert.assertEquals(user.getLogin(), userFind.getLogin());
        Assert.assertEquals(user.getLogin(), userLogin);
        Assert.assertThrows(UserNameAlreadyExistsException.class, () -> userService.create(userLogin, userPassword));
    }

    @Test
    public void createEmail() {
        @Nullable final String email = null;
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create("", "", ""));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(null, null, email));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create(userLogin, "", ""));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create(userLogin, null, email));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.create(userLogin, userPassword, ""));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.create(userLogin, userPassword, email));
        Assert.assertEquals(0, userService.count());
        @NotNull User user = userService.create(userLogin, userPassword, userEmail);
        Assert.assertNotNull(user);
        Assert.assertEquals(1, userService.count());
        @Nullable User userFind = userService.findOneById(user.getId());
        Assert.assertNotNull(userFind);
        Assert.assertEquals(user, userFind);
        Assert.assertEquals(user.getLogin(), userFind.getLogin());
        Assert.assertEquals(user.getEmail(), userFind.getEmail());
        Assert.assertEquals(user.getLogin(), userLogin);
        Assert.assertEquals(user.getEmail(), userEmail);
        Assert.assertThrows(
                UserNameAlreadyExistsException.class, () -> userService.create(userLogin, userPassword, userEmail)
        );
        Assert.assertThrows(
                EmailAlreadyExistsException.class,
                () -> userService.create(UUID.randomUUID().toString(), userPassword, userEmail)
        );
    }

    @Test
    public void createRole() {
        @Nullable final Role role = null;
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create("", "", role));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(null, null, role));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create(userLogin, "", role));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create(userLogin, null, role));
        Assert.assertThrows(RoleEmptyException.class, () -> userService.create(userLogin, userPassword, role));
        Assert.assertEquals(0, userService.count());
        @NotNull User user = userService.create(userLogin, userPassword, Role.ADMIN);
        Assert.assertNotNull(user);
        Assert.assertEquals(1, userService.count());
        @Nullable User userFind = userService.findOneById(user.getId());
        Assert.assertNotNull(userFind);
        Assert.assertEquals(user, userFind);
        Assert.assertEquals(user.getLogin(), userFind.getLogin());
        Assert.assertEquals(user.getRole(), userFind.getRole());
        Assert.assertEquals(user.getLogin(), userLogin);
        Assert.assertEquals(user.getRole(), Role.ADMIN);
        Assert.assertThrows(
                UserNameAlreadyExistsException.class, () -> userService.create(userLogin, userPassword, Role.ADMIN)
        );
    }

    @Test
    public void isLoginExists() {
        Assert.assertEquals(0, userService.count());
        Assert.assertFalse(userService.isLoginExists(""));
        Assert.assertFalse(userService.isLoginExists(null));
        Assert.assertFalse(userService.isLoginExists(userLogin));
        userService.create(userLogin, userPassword);
        Assert.assertTrue(userService.isLoginExists(userLogin));
    }

    @Test
    public void isEmailExists() {
        Assert.assertEquals(0, userService.count());
        Assert.assertFalse(userService.isLoginExists(""));
        Assert.assertFalse(userService.isLoginExists(null));
        Assert.assertFalse(userService.isLoginExists(userLogin));
        userService.create(userLogin, userPassword, userEmail);
        Assert.assertTrue(userService.isEmailExists(userEmail));
        Assert.assertFalse(userService.isEmailExists(UUID.randomUUID().toString()));
    }

    @Test
    public void findByLogin() {
        Assert.assertEquals(0, userService.count());
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(""));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(null));
        Assert.assertNull(userService.findByLogin(userLogin));
        @NotNull User user = userService.create(userLogin, userPassword, userEmail);
        @Nullable User userFind = userService.findOneById(user.getId());
        Assert.assertNotNull(userFind);
        Assert.assertEquals(user, userFind);
        Assert.assertEquals(user.getLogin(), userFind.getLogin());
        Assert.assertEquals(user.getEmail(), userFind.getEmail());
        Assert.assertNull(userService.findByLogin(UUID.randomUUID().toString()));
    }

    @Test
    public void findByEmail() {
        Assert.assertEquals(0, userService.count());
        Assert.assertThrows(EmailEmptyException.class, () -> userService.findByEmail(""));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.findByEmail(null));
        Assert.assertNull(userService.findByEmail(userEmail));
        @NotNull User user = userService.create(userLogin, userPassword, userEmail);
        @Nullable User userFind = userService.findByEmail(user.getEmail());
        Assert.assertNotNull(userFind);
        Assert.assertEquals(user, userFind);
        Assert.assertEquals(user.getLogin(), userFind.getLogin());
        Assert.assertEquals(user.getEmail(), userFind.getEmail());
        Assert.assertNull(userService.findByEmail(UUID.randomUUID().toString()));
    }

    @Test
    public void remove() {
        Assert.assertEquals(0, userService.count());
        Assert.assertNull(userService.remove(null));
        @NotNull User user = userService.create(userLogin, userPassword, userEmail);
        Assert.assertEquals(1, userService.count());
        @NotNull String userId = user.getId();
        projectService.create(userId, projectName);
        taskService.create(userId, taskName);
        projectService.create(UUID.randomUUID().toString(), projectName);
        taskService.create(UUID.randomUUID().toString(), taskName);
        Assert.assertEquals(2, projectService.count());
        Assert.assertEquals(2, taskService.count());
        Assert.assertEquals(1, projectService.count(userId));
        Assert.assertEquals(1, taskService.count(userId));
        @Nullable User userRemove = userService.remove(user);
        Assert.assertNotNull(userRemove);
        Assert.assertEquals(user, userRemove);
        Assert.assertEquals(user.getLogin(), userRemove.getLogin());
        Assert.assertEquals(1, projectService.count());
        Assert.assertEquals(1, taskService.count());
        Assert.assertEquals(0, projectService.count(userId));
        Assert.assertEquals(0, taskService.count(userId));
    }

    @Test
    public void removeByLogin() {
        Assert.assertEquals(0, userService.count());
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin(""));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin(null));
        @NotNull User user = userService.create(userLogin, userPassword, userEmail);
        Assert.assertEquals(1, userService.count());
        @NotNull String userId = user.getId();
        projectService.create(userId, projectName);
        taskService.create(userId, taskName);
        projectService.create(UUID.randomUUID().toString(), projectName);
        taskService.create(UUID.randomUUID().toString(), taskName);
        Assert.assertEquals(2, projectService.count());
        Assert.assertEquals(2, taskService.count());
        Assert.assertEquals(1, projectService.count(userId));
        Assert.assertEquals(1, taskService.count(userId));
        @Nullable User userRemove = userService.removeByLogin(user.getLogin());
        Assert.assertNotNull(userRemove);
        Assert.assertEquals(user, userRemove);
        Assert.assertEquals(user.getLogin(), userRemove.getLogin());
        Assert.assertEquals(1, projectService.count());
        Assert.assertEquals(1, taskService.count());
        Assert.assertEquals(0, projectService.count(userId));
        Assert.assertEquals(0, taskService.count(userId));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.removeByLogin(user.getLogin()));
    }

    @Test
    public void setPassword() {
        Assert.assertEquals(0, userService.count());
        @NotNull User user = userService.create(userLogin, userPassword, userEmail);
        @NotNull String userId = user.getId();
        @NotNull String newPassword = UUID.randomUUID().toString();
        Assert.assertThrows(UserIdEmptyException.class, () -> userService.setPassword("", ""));
        Assert.assertThrows(UserIdEmptyException.class, () -> userService.setPassword(null, null));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword(userId, null));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword(userId, ""));
        Assert.assertNull(userService.setPassword(UUID.randomUUID().toString(), newPassword));
        @Nullable User userPass = userService.setPassword(userId, newPassword);
        Assert.assertNotNull(userPass);
        Assert.assertEquals(user, userPass);
        Assert.assertEquals(HashUtil.salt(propertyService, newPassword), userPass.getPasswordHash());
    }

    @Test
    public void updateUser() {
        Assert.assertEquals(0, userService.count());
        @NotNull User user = userService.create(userLogin, userPassword, userEmail);
        @NotNull String userId = user.getId();
        @NotNull String newFirstName = UUID.randomUUID().toString();
        @NotNull String newLastName = UUID.randomUUID().toString();
        @NotNull String newMiddleName = UUID.randomUUID().toString();
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> userService.updateUser("", "", "", "")
        );
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> userService.updateUser(null, null, null, null)
        );
        Assert.assertNull(
                userService.updateUser(UUID.randomUUID().toString(), "", "", "")
        );
        @Nullable User userPass = userService.updateUser(userId, newFirstName, newLastName, newMiddleName);
        Assert.assertNotNull(userPass);
        Assert.assertEquals(user, userPass);
        Assert.assertEquals(newFirstName, userPass.getFirstName());
        Assert.assertEquals(newLastName, userPass.getLastName());
        Assert.assertEquals(newMiddleName, userPass.getMiddleName());
    }

    @Test
    public void lockUserByLogin() {
        Assert.assertEquals(0, userService.count());
        @NotNull User user = userService.create(userLogin, userPassword, userEmail);
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(""));
        Assert.assertThrows(
                UserNotFoundException.class, () -> userService.lockUserByLogin(UUID.randomUUID().toString())
        );
        Assert.assertFalse(user.getLocked());
        userService.lockUserByLogin(user.getLogin());
        Assert.assertTrue(user.getLocked());
    }

    @Test
    public void unlockUserByLogin() {
        Assert.assertEquals(0, userService.count());
        @NotNull User user = userService.create(userLogin, userPassword, userEmail);
        user.setLocked(true);
        Assert.assertThrows(LoginEmptyException.class, () -> userService.unlockUserByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.unlockUserByLogin(""));
        Assert.assertThrows(
                UserNotFoundException.class, () -> userService.unlockUserByLogin(UUID.randomUUID().toString())
        );
        Assert.assertTrue(user.getLocked());
        userService.unlockUserByLogin(user.getLogin());
        Assert.assertFalse(user.getLocked());
    }

}
