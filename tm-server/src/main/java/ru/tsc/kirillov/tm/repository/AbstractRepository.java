package ru.tsc.kirillov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.repository.IRepository;
import ru.tsc.kirillov.tm.model.AbstractModel;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected List<M> models = new Vector<>();

    @NotNull
    protected Predicate<M> predicateById(@NotNull final String id) {
        return user -> id.equals(user.getId());
    }

    @NotNull
    @Override
    public M add(@NotNull final M model) {
        models.add(model);
        return model;
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        this.models.addAll(models);
        return models;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) {
        clear();
        return add(models);
    }

    @Override
    public void clear() {
        models.clear();
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return models;
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final Comparator<M> comparator) {
        return models
                .stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String id) {
        return models
                .stream()
                .filter(predicateById(id))
                .findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public M findOneByIndex(@NotNull final Integer index) {
        return models.get(index);
    }

    @NotNull
    @Override
    public M remove(@NotNull final M model) {
        models.remove(model);
        return model;
    }

    @Override
    public void removeAll(@Nullable final Collection<M> collection) {
        if (collection == null) return;
        models.removeAll(collection);
    }

    @Nullable
    @Override
    public M removeById(@NotNull final String id) {
        Optional<M> model = Optional.ofNullable(findOneById(id));
        model.ifPresent(models::remove);
        return model.orElse(null);
    }

    @NotNull
    @Override
    public M removeByIndex(@NotNull final Integer index) {
        @NotNull M model = findOneByIndex(index);
        models.remove(model);
        return model;
    }

    @Override
    public long count() {
        return models.size();
    }

}
