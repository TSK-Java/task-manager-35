package ru.tsc.kirillov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.repository.ITaskRepository;
import ru.tsc.kirillov.tm.model.Task;

import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @NotNull
    protected Predicate<Task> predicateByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return task -> task.getProjectId() != null
                && userId.equals(task.getUserId())
                && projectId.equals(task.getProjectId());
    }

    @NotNull
    @Override
    public Task create(@Nullable final String userId, @NotNull final String name) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    public Task create(@Nullable final String userId, @NotNull final String name, @NotNull final String description) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        task.setDescription(description);
        return add(task);
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || projectId == null) return Collections.emptyList();
        return models
                .stream()
                .filter(predicateByUserIdAndProjectId(userId, projectId))
                .collect(Collectors.toList());
    }

    @Override
    public void removeAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        findAllByProjectId(userId, projectId)
            .forEach(t -> removeById(userId, t.getId()));
    }

    @Override
    public void removeAllByProjectList(@Nullable final String userId, @Nullable final String[] projects) {
        Stream.of(projects)
                .filter(p -> p != null && !p.isEmpty())
                .forEach(p -> removeAllByProjectId(userId, p));
    }

}
