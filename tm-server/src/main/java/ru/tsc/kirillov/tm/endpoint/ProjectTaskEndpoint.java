package ru.tsc.kirillov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.kirillov.tm.api.service.IProjectTaskService;
import ru.tsc.kirillov.tm.api.service.IServiceLocator;
import ru.tsc.kirillov.tm.dto.request.*;
import ru.tsc.kirillov.tm.dto.response.*;
import ru.tsc.kirillov.tm.model.Session;
import ru.tsc.kirillov.tm.util.NumberUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "ru.tsc.kirillov.tm.api.endpoint.IProjectTaskEndpoint")
public final class ProjectTaskEndpoint extends AbstractEndpoint implements IProjectTaskEndpoint {

    public ProjectTaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectClearResponse clearProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectClearRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        getProjectTaskService().clear(userId);
        return new ProjectClearResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectRemoveByIdResponse removeProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectRemoveByIdRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        getProjectTaskService().removeProjectById(userId, id);
        return new ProjectRemoveByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectRemoveByIndexResponse removeProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectRemoveByIndexRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = NumberUtil.fixIndex(request.getIndex());
        getProjectTaskService().removeProjectByIndex(userId, index);
        return new ProjectRemoveByIndexResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectBindTaskByIdResponse bindTaskToProjectId(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectBindTaskByIdRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
        return new ProjectBindTaskByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectUnbindTaskByIdResponse unbindTaskToProjectId(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectUnbindTaskByIdRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        getProjectTaskService().unbindTaskToProject(userId, projectId, taskId);
        return new ProjectUnbindTaskByIdResponse();
    }

}
