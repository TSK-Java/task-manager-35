package ru.tsc.kirillov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.repository.IUserOwnedRepository;
import ru.tsc.kirillov.tm.model.AbstractUserOwnedModel;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M>
        implements IUserOwnedRepository<M> {

    @NotNull
    protected Predicate<M> predicateByUserId(@NotNull final String userId) {
        return user -> userId.equals(user.getUserId());
    }

    @NotNull
    protected Predicate<M> predicateByIdAndUserId(@NotNull final String id, @NotNull final String userId) {
        return user -> id.equals(user.getId()) && userId.equals(user.getUserId());
    }

    @Nullable
    @Override
    public M add(@Nullable final String userId, @NotNull final M model) {
        if (userId == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @Override
    public void clear(@Nullable final String userId) {
        final List<M> models = findAll(userId);
        removeAll(models);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) {
        if (userId == null) return Collections.emptyList();
        return models
                .stream()
                .filter(predicateByUserId(userId))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId, @NotNull final Comparator<M> comparator) {
        if (userId == null) return Collections.emptyList();
        return models
                .stream()
                .filter(predicateByUserId(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return false;
        return models
                .stream()
                .anyMatch(predicateByIdAndUserId(id, userId));
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        return models
                .stream()
                .filter(predicateByIdAndUserId(id, userId))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null) return null;
        if (index == null) return null;
        @NotNull List<M> list = findAll(userId);
        if (list.isEmpty()) return null;
        return list.get(index);
    }

    @Nullable
    @Override
    public M remove(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || model == null) return null;
        return removeById(userId, model.getId());
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        final Optional<M> model = Optional.ofNullable(findOneById(userId, id));
        model.ifPresent(models::remove);
        return model.orElse(null);
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final String userId, @NotNull final Integer index) {
        @Nullable M model = findOneByIndex(userId, index);
        models.remove(model);
        return model;
    }

    @Override
    public long count(@Nullable final String userId) {
        if (userId == null) return 0;
        return models
                .stream()
                .filter(predicateByUserId(userId))
                .count();
    }

}
