package ru.tsc.kirillov.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.api.service.ITokenService;

@Getter
@Setter
public final class TokenService implements ITokenService {

    @NotNull
    private String token;

}
