package ru.tsc.kirillov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.request.DataXmlJaxbLoadRequest;
import ru.tsc.kirillov.tm.enumerated.Role;

public final class DataLoadXmlJaxbCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String getName() {
        return "data-load-xml-jaxb";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Загрузить состояние приложения из xml файла (JAXB API)";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[Загрузка состояния приложения из xml файла (JAXB API)]");
        getDomainEndpoint().loadDataXmlJaxb(new DataXmlJaxbLoadRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
