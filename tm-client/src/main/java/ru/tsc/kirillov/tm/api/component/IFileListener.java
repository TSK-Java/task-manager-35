package ru.tsc.kirillov.tm.api.component;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.event.FileEvent;

import java.util.EventListener;

@FunctionalInterface
public interface IFileListener extends EventListener {

    void onEvent(@NotNull final FileEvent event);

}
